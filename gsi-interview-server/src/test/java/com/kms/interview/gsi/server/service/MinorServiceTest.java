package com.kms.interview.gsi.server.service;

import com.kms.interview.gsi.server.exception.ServiceException;
import com.kms.interview.gsi.server.service.impl.MinorServiceImpl;
import com.kms.interview.gsi.server.util.DateUtil;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class MinorServiceTest {

    private static MinorService service;

    @BeforeClass
    public static void beforeClass() {
        service = new MinorServiceImpl();
    }

    @Test
    public void isMinorByBirthDate_AgeLessThan18_True() throws Exception {
        Assert.assertEquals(true,
                service.isMinorByBirthDate(DateUtil.toDate("10/11/2000")));
    }

    @Test
    public void isMinorByBirthDate_AgeGreaterThan18_False() throws Exception {
        Assert.assertEquals(false,
                service.isMinorByBirthDate(DateUtil.toDate("10/11/1986")));
    }

    @Test
    public void isMinorByBirthDate_AgeIs18_False() throws Exception {
        Assert.assertEquals(true,
                service.isMinorByBirthDate(DateUtil.toDate("10/11/1998")));
    }

    @Test
    public void isMinorByBirthDate_InvalidAge_False() throws Exception {
        Assert.assertEquals(true,
                service.isMinorByBirthDate(DateUtil.toDate("10/11/2115")));
    }

    @Test(expected = ServiceException.class)
    public void isMinorByBirthDate_BirthDateIsNull_False() throws Exception {
        Assert.assertEquals(true, service.isMinorByBirthDate(null));
    }
}
