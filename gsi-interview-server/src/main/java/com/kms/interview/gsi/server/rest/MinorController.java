package com.kms.interview.gsi.server.rest;

import com.kms.interview.gsi.server.service.MinorService;
import com.kms.interview.gsi.server.util.DateUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MinorController {

    private static final Logger log = Logger.getLogger(MinorController.class);

    @Autowired
    private MinorService service;

    @RequestMapping(value = "isMinor",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<MinorRESTResponse> isMinor(
            @RequestBody MinorRESTRequest request) {

        MinorRESTResponse res = new MinorRESTResponse();

        for (String birthDate : request.getBirthDateList()) {
            try {
                if (service.isMinorByBirthDate(DateUtil.toDate(birthDate))) {
                    res.addMinor(birthDate);
                } else {
                    res.addNonMinor(birthDate);
                }
            } catch (Exception ex) {
                log.debug(ex.getMessage(), ex);
                res.addError(birthDate, ex.getMessage());
            }
        }

        return ResponseEntity.ok(res);
    }
}
