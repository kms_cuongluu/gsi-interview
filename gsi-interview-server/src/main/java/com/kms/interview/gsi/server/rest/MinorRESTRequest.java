package com.kms.interview.gsi.server.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MinorRESTRequest {
    private List<String> birthDateList;

    public MinorRESTRequest() {
        birthDateList = new ArrayList<>();
    }

    public List<String> getBirthDateList() {
        return birthDateList;
    }

    public void setBirthDateList(List<String> birthDateList) {
        this.birthDateList = birthDateList;
    }

    public void addBirthDate(String birthDate) {
        birthDateList.add(birthDate);
    }
}
