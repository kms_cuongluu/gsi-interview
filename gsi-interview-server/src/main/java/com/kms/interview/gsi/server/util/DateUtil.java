package com.kms.interview.gsi.server.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {
    public static final SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("MM/dd/yyyy");

    private DateUtil() {
    }

    public static Date toDate(String text) throws Exception {
        return DATE_FORMAT.parse(text);
    }
}
