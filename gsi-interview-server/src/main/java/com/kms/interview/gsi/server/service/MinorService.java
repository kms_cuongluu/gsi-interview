package com.kms.interview.gsi.server.service;

import com.kms.interview.gsi.server.exception.ServiceException;

import java.util.Date;

public interface MinorService {
    boolean isMinorByBirthDate(Date birthDate) throws ServiceException;
}
