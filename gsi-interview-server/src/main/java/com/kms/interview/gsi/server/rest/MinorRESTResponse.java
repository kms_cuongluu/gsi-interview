package com.kms.interview.gsi.server.rest;

import java.util.ArrayList;
import java.util.List;

public class MinorRESTResponse {

    private List<MinorStatus> birthDateList;

    public MinorRESTResponse() {
        birthDateList = new ArrayList<>();
    }

    public List<MinorStatus> getBirthDateList() {
        return birthDateList;
    }

    public void setBirthDateList(List<MinorStatus> birthDateList) {
        this.birthDateList = birthDateList;
    }

    public void addMinor(final String birthDate) {
        birthDateList.add(new MinorStatus(birthDate, true, null));
    }

    public void addNonMinor(final String birthDate) {
        birthDateList.add(new MinorStatus(birthDate, false, null));
    }

    public void addError(final String birthDate, final String error) {
        birthDateList.add(new MinorStatus(birthDate, null, error));
    }

    static class MinorStatus {
        private String birthDate;

        private Boolean isMinor;

        private String error;

        public MinorStatus() {
        }

        public MinorStatus(final String birthDate,
                           final Boolean isMinor,
                           final String error) {
            this.birthDate = birthDate;
            this.isMinor = isMinor;
            this.error = error;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        public String getBirthDate() {
            return birthDate;
        }

        public void setBirthDate(String birthDate) {
            this.birthDate = birthDate;
        }

        public Boolean getMinor() {
            return isMinor;
        }

        public void setMinor(Boolean minor) {
            isMinor = minor;
        }
    }
}
