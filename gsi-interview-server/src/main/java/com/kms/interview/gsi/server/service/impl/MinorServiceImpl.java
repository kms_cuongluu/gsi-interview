package com.kms.interview.gsi.server.service.impl;

import com.kms.interview.gsi.server.exception.ServiceException;
import com.kms.interview.gsi.server.service.MinorService;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class MinorServiceImpl implements MinorService {

    private static final int MINOR_AGE_MAX = 18;

    @Override
    public boolean isMinorByBirthDate(final Date birthDate)
            throws ServiceException {

        if (birthDate == null)
            throw new ServiceException("Illegal argument!");

        Date now = new Date();

        Years age = Years.yearsBetween(new DateTime(birthDate),
                new DateTime(now));

        if (age.getYears() < MINOR_AGE_MAX)
            return true;

        return false;
    }
}
