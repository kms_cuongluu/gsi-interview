GSI Interview Project
====================


TECHNOLOGY
================================
	- Java 1.7
	- Maven
	- Spring REST and Boot
	- Junit

Explanation:
	- Use RESTfull with http protocol, no authentication, no centralized database, the service is 100% stateless so it is scalable

	- Use Spring framework for RESTfull services. We can add more Spring modules easily later (spring-security, spring-jpa, spring-data…)

	- Don't use cache to improve performance because the logic is simple. We should consider to apply cache when the service connects to database, disk,...


SOURCE CODE
================================
There are 2 modules:
	1. gsi-interview-server: RESTfull service code
	2. gsi-interview-dist: binary distribution. We collect jar files, config file, bash/batch scripts in the module. After we build source code, use gsi-interview-dist.tar in the module to deploy/run the service.

BUILD
================================

```
#!script
  > CD [project source code directory]
  > mvn clean package
```

After build, we should have a distribution file in the target directory.

```
#!script
  > CD [project source code directory]\gsi-interview-dist\target\gsi-interview-dist
  > ls -l
  >   bin
  >       server					// Linux bash script
  >       server.bat				// Windows batch script
  >
  >   lib							// All jar files
  >       
  >   log							// All log file when running the service
  > 
  >   application.properties		// Config file (eg: http port)
```

BUILD
================================
Check gsi-interview-dist directory in /[project]/gsi-interview-dist/target

	- You can change config (eg: http port) in application.properties

	- Windows: run below command
		./bin/server.bat

	- Linux: run below command
		./bin/server start

		*to check status: ./bin/server status
		*to stop the service: ./bin/server stop


